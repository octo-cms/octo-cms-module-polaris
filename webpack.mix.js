const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');
require('laravel-mix-purgecss');

mix.setPublicPath('../../public').mergeManifest();

const themePath = '../../public/assets_polaris';

mix.copyDirectory(__dirname + '/Resources/assets/dist/css', themePath + '/css')
    .copyDirectory(__dirname + '/Resources/assets/dist/img', themePath + '/img')
    .copyDirectory(__dirname + '/Resources/assets/dist/js', themePath + '/js');

mix.js(__dirname + '/Resources/assets/js/scripts.js', themePath + '/js/scripts.js');

mix.scripts([
    themePath + '/js/aos.js',
    themePath + '/js/scripts.js',
], themePath + '/js/all-scripts.min.js');

mix.sass(
    __dirname + '/Resources/assets/scss/theme.scss',
    themePath + '/css/main.min.css'
)
   .purgeCss({

        enabled: false,
        content: [
           path.join(__dirname, '/Resources/views/**/*.php'),
           path.join(__dirname, '/Resources/views/!**!/!**/!*.php'),
        ],
        whitelistPatterns: [/aos/, /flickity/, /bg-/, /text-/, /transform-/, /divider-/,/divider-/,/dropdown(.*)/,/row/,/col-auto/,/fade-page/,/nav(.*)/],
    })

if (mix.inProduction()) {
    mix.version();
}

