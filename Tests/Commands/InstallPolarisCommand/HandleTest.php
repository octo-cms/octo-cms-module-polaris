<?php

namespace OctoCmsModule\Polaris\Tests\Commands\InstallPolarisCommand;

use OctoCmsModule\Polaris\Console\PolarisEntityBlocks;
use OctoCmsModule\Polaris\Console\PolarisHtmlBlocks;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 *
 * @package OctoCmsModule\Polaris\Tests\Commands\InstallPolarisCommand
 */
class HandleTest extends TestCase
{


    public function test_installPolarisCommand()
    {
        $this->artisan('install:polaris')
            ->expectsOutput('Running Install Polaris Command ...')
            ->expectsOutput('Creating BlockHtml ...')
            ->expectsOutput('Creating BlockEntity ...');

        foreach (PolarisHtmlBlocks::BLOCKS as $block) {
            $this->assertDatabaseHas('block_html', [
                'module'       => 'Polaris',
                'blade'        => $block['blade'],
                'instructions' => $block['instructions'],
                'settings'     => serialize($block['values']),
                'layout'       => serialize($block['layout']),
            ]);
        }

        foreach (PolarisEntityBlocks::BLOCKS as $block) {
            $this->assertDatabaseHas('block_entities', [
                'module'       => 'Polaris',
                'blade'        => $block['blade'],
                'entity'       => $block['entity'],
                'instructions' => $block['instructions'],
                'settings'     => serialize($block['values']),
                'layout'       => serialize($block['layout']),
            ]);
        }
    }
}
