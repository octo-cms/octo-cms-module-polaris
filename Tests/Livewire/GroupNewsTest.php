<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Carbon\Carbon;
use Livewire\Livewire;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Class GroupNewsTest
 *
 * @package OctoCmsModule\Polaris\Tests\Livewire
 */
class GroupNewsTest extends TestCase
{

    public function test_getNewsArrayWithPage()
    {
        $vv = News::factory()->count(2)
            ->has(Page::factory()->has(PageLang::factory()->state(['lang' => 'it', 'url' => 'url-test'])))
            ->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'it',
                            'title'             => 'titolo-' . $news->id,
                            'short_description' => 'descrizione-breve-' . $news->id,
                        ];
                    }))->create(['date' => Carbon::now(), 'author' => 'roberto']);

        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 1],
                1 => ['id' => 2],
            ],
        ];

        Livewire::test('group-news', ['targets' => $targets])
            ->assertSet('news', [
                [
                    'id'                => 1,
                    'title'             => 'titolo-1',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-1',
                    'picture'           => null,
                    'author'            => 'roberto',
                    'url'               => 'url-test'
                ],
                [
                    'id'                => 2,
                    'title'             => 'titolo-2',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-2',
                    'picture'           => null,
                    'author'            => 'roberto',
                    'url'               => 'url-test'
                ],
            ]);

    }

    public function test_getNewsArray()
    {

        News::factory()->count(5)
            ->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'it',
                            'title'             => 'titolo-' . $news->id,
                            'short_description' => 'descrizione-breve-' . $news->id,
                        ];
                    })
            )->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'en',
                            'title'             => 'title-' . $news->id,
                            'short_description' => 'short-description-' . $news->id,
                        ];
                    })
            )
            ->create(['date' => Carbon::now(), 'author' => 'roberto']);

        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 1],
                1 => ['id' => 2],
                2 => ['id' => 3],
                3 => ['id' => 4],
                4 => ['id' => 5],
            ],
        ];

        Livewire::test('group-news', ['targets' => $targets])
            ->assertSet('news', [
                [
                    'id'                => 1,
                    'title'             => 'titolo-1',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-1',
                    'picture'           => null,
                    'author'            => 'roberto',
                    'url'               => ''
                ],
                [
                    'id'                => 2,
                    'title'             => 'titolo-2',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-2',
                    'picture'           => null,
                    'author'            => 'roberto',
                    'url'               => ''
                ],
                [
                    'id'                => 3,
                    'title'             => 'titolo-3',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-3',
                    'picture'           => null,
                    'author'            => 'roberto',
                    'url'               => ''
                ],
                [
                    'id'                => 4,
                    'title'             => 'titolo-4',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-4',
                    'picture'           => null,
                    'author'            => 'roberto',
                    'url'               => ''
                ],
            ]);
    }
}
