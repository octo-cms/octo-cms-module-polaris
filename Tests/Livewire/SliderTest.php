<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SliderTest
 *
 * @package OctoCmsModule\Polaris\Tests\Livewire
 */
class SliderTest extends TestCase
{


    public function test_getNewsArray()
    {
        Slide::factory()->create([
            'caption'      => 'caption-1',
            'sub_caption'  => 'sub_caption-1',
            'action_label' => 'action_label-1',
            'action_link'  => 'action_link-1',
        ]);

        Slide::factory()->create([
            'caption'      => 'caption-2',
            'sub_caption'  => 'sub_caption-2',
            'action_label' => 'action_label-2',
            'action_link'  => 'action_link-2',
        ]);


        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 1],
                1 => ['id' => 2],
            ],
        ];

        Livewire::test('slider', ['targets' => $targets])
            ->assertSet('slides', [
                [
                    'id'           => 1,
                    'caption'      => 'caption-1',
                    'sub_caption'  => 'sub_caption-1',
                    'action_label' => 'action_label-1',
                    'action_link'  => 'action_link-1',
                    'picture'      => null,
                ],
                [
                    'id'           => 2,
                    'caption'      => 'caption-2',
                    'sub_caption'  => 'sub_caption-2',
                    'action_label' => 'action_label-2',
                    'action_link'  => 'action_link-2',
                    'picture'      => null,
                ],
            ]);
    }
}
