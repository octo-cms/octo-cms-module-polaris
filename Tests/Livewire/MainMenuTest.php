<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Faker\Factory as Faker;
use Livewire\Livewire;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Class ListNewsTest
 *
 * @package OctoCmsModule\Polaris\Tests\Livewire
 */
class MainMenuTest extends TestCase
{

    public function test_MainMenu()
    {
        $faker = Faker::create();


        /** @var Page $page */
        $page = Page::factory()->has(PageLang::factory()->state(
            function (array $attributes, Page $page) {
                return [
                    'lang'             => 'it',
                    'meta_title'       => 'meta-titolo-' . $page->id,
                    'meta_description' => 'meta-descrizione-' . $page->id,
                    'url'              => 'url-test',
                ];
            }))->create([
            'name' => 'homepage',
            'type' => Page::TYPE_HOMEPAGE,
        ]);

        /** @var Menu $menu */
        $menu = Menu::factory()->create([
            'name'  => Menu::MAIN_MENU,
            'blade' => "main",
        ]);

        // First level
        $m1 = MenuItem::factory()->create([
            'menu_id' => $menu->id,
            'type'    => MenuItem::TYPE_EXTERNAL,
            'data'    => ['link' => $faker->url],
        ]);

        $m2 = MenuItem::factory()->create([
            'menu_id' => $menu->id,
            'type'    => MenuItem::TYPE_EXTERNAL,
            'data'    => ['link' => $faker->url],
        ]);

        // Second Level
        $m3 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m1->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        $m4 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m1->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        $m5 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m2->id,
            'type'      => MenuItem::TYPE_PAGE,
            'data'      => ['page' => ['value' => $page->id]],
        ]);

        $m6 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m2->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        // Third level

        $m7 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m6->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        $m8 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m6->id,
            'type'      => MenuItem::TYPE_PAGE,
            'data'      => ['page' => ['value' => $page->id]],
        ]);


        Livewire::test('main-menu')
            ->assertSee($m1['data']['link'])
            ->assertSee($m2['data']['link'])
            ->assertSee($m3['data']['link'])
            ->assertSee($m4['data']['link'])
            ->assertSee($m6['data']['link'])
            ->assertSee($m7['data']['link'])
            ->assertStatus(200);
    }
}
