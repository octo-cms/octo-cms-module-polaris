<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Illuminate\Support\Facades\Queue;
use Livewire\Livewire;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Jobs\CreateActiveCampaignJob;
use OctoCmsModule\Polaris\Http\Livewire\EmailForm;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class EmailFormTest
 *
 * @package OctoCmsModule\Polaris\Tests\Livewire
 */
class EmailFormTest extends TestCase
{


    public function test_emailForm()
    {
        Queue::fake();

        Livewire::test('email-form', ['data' => []])
            ->set('name', 'firstName')
            ->set('email', 'info@mail.com')
            ->set('message', 'message')
            ->set('privacy', true)
            ->call('saveRegistry');

        $this->assertDatabaseHas('registry', [
            'id'   => 1,
            'type' => 'private',
        ]);

        $this->assertDatabaseHas('emails', [
            'label'          => 'default',
            'default'        => true,
            'emailable_id'   => 1,
            'emailable_type' => 'Registry'
        ]);

        $this->assertDatabaseHas('private_registry', [
            'registry_id' => 1,
            'name'        => 'firstName',
        ]);

        Queue::assertPushed(CreateActiveCampaignJob::class);
    }
}
