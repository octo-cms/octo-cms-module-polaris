<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class TestimonialsTest
 *
 * @package OctoCmsModule\Polaris\Tests\Livewire
 */
class TestimonialsTest extends TestCase
{


    public function test_getTestimonials()
    {
        factory(Testimonial::class, 4)->create([
            'author' => 'author',
        ])
            ->each(function (Testimonial $testimonial) {
                factory(TestimonialLang::class, 1)->create([
                    'testimonial_id' => $testimonial->id,
                    'lang'           => 'it',
                    'job'            => 'lavoro-' . $testimonial->id,
                    'text'           => 'testo-' . $testimonial->id,
                ]);
                factory(TestimonialLang::class, 1)->create([
                    'testimonial_id' => $testimonial->id,
                    'lang'           => 'en',
                    'job'            => 'job-' . $testimonial->id,
                    'text'           => 'text-' . $testimonial->id,
                ]);
            });


        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 3],
                1 => ['id' => 1],
                2 => ['id' => 2],
                3 => ['id' => 4],
            ],
        ];

        Livewire::test('testimonials', ['targets' => $targets])
            ->assertSet('testimonials', [
                [
                    'id'      => 3,
                    'author'  => 'author',
                    'job'     => 'lavoro-3',
                    'text'    => 'testo-3',
                    'picture' => null,
                ],
                [
                    'id'      => 1,
                    'author'  => 'author',
                    'job'     => 'lavoro-1',
                    'text'    => 'testo-1',
                    'picture' => null,
                ],
                [
                    'id'      => 2,
                    'author'  => 'author',
                    'job'     => 'lavoro-2',
                    'text'    => 'testo-2',
                    'picture' => null,
                ],
            ]);
    }
}
