<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Carbon\Carbon;
use Livewire\Livewire;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 *
 * Class GetCustomEntitiesTest
 *
 * @package OctoCmsModule\Core\Tests\Utils\LivewireUtils
 */
class SingleNewsTest extends TestCase
{


    public function test_getCustomEntities()
    {
        News::factory()
            ->has(NewsLang::factory()
                ->state([
                    'lang'              => 'it',
                    'title'             => 'title',
                    'short_description' => 'short_description',
                ])
            )->create(['date' => Carbon::now(), 'author' => 'roberto']);

        News::factory()->count(2)->create();

        $targets = [
            'type'   => 'custom',
            'values' =>
                [0 => ['id' => 1]],
        ];

        Livewire::test('single-news', ['targets' => $targets])
            ->assertSet('news', [
                'id'                => 1,
                'title'             => 'title',
                'date'              => Carbon::now()->format('d/m/Y'),
                'short_description' => 'short_description',
                'picture'           => null,
                'author'            => 'roberto',
                'url'               => ''
            ]);
    }
}
