<?php

namespace OctoCmsModule\Polaris\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ListPagesTest
 *
 * @package OctoCmsModule\Polaris\Tests\Livewire
 */
class ListPagesTest extends TestCase
{


    public function test_getPageArray()
    {
        Page::factory()
            ->count(4)
            ->has(PageLang::factory()->state(
                function (array $attributes, Page $page) {
                    return [
                        'lang'             => 'it',
                        'meta_title'       => 'meta-titolo-' . $page->id,
                        'meta_description' => 'meta-descrizione-' . $page->id,
                        'url'              => 'url-it-' . $page->id,
                    ];
                })
            )->has(PageLang::factory()->state(
                function (array $attributes, Page $page) {
                    return [
                        'lang'             => 'en',
                        'meta_title'       => 'meta-title-' . $page->id,
                        'meta_description' => 'meta-description-' . $page->id,
                        'url'              => 'url-en-' . $page->id,
                    ];
                })
            )
            ->create();

        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 1],
                1 => ['id' => 2],
                2 => ['id' => 3],
                3 => ['id' => 4],
            ],
        ];

        Livewire::test('list-pages', ['targets' => $targets])
            ->assertSet('pages', [
                [
                    'id'               => 1,
                    'meta_title'       => 'meta-titolo-1',
                    'meta_description' => 'meta-descrizione-1',
                    'url'              => 'url-it-1',
                    'picture'          => null,
                ],
                [
                    'id'               => 2,
                    'meta_title'       => 'meta-titolo-2',
                    'meta_description' => 'meta-descrizione-2',
                    'url'              => 'url-it-2',
                    'picture'          => null,
                ],
                [
                    'id'               => 3,
                    'meta_title'       => 'meta-titolo-3',
                    'meta_description' => 'meta-descrizione-3',
                    'url'              => 'url-it-3',
                    'picture'          => null,
                ],
            ]);
    }
}
