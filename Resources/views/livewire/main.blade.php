@php($unique= uniqid())
<div class="d-flex align-items-center order-lg-3">
    <!-- Nav toggler for mobile -->
    <button aria-expanded="false"
            aria-label="Toggle navigation"
            class="navbar-toggler"
            data-target="#navigation-menu-{{$unique}}"
            data-controls="navigation-menu-{{$unique}}"
            data-toggle="collapse"
            type="button">

        <img alt="Navbar Toggler Open Icon" class="navbar-toggler-open icon icon-sm"
             src="{{ asset('assets_polaris/img/icons/interface/icon-menu.svg')}}">
        <img alt="Navbar Toggler Close Icon" class="navbar-toggler-close icon icon-sm"
             src="{{ asset('assets_polaris/img/icons/interface/icon-x.svg')}}">
    </button>
</div>

<div class="navbar-collapse order-3 order-lg-2 justify-content-lg-end collapse" id="navigation-menu-{{$unique}}">
    <ul class="navbar-nav my-3 my-lg-0">
        {!!$this->content !!}
    </ul>
</div>


