<div class="col">
    <div class="d-lg-flex flex-column flex-fill controls-hover"
         data-flickity='{ "imagesLoaded": true, "wrapAround":true, "pageDots":false, "autoPlay":true }'>
        @foreach($this->slides as $slide)
            <div class="carousel-cell text-center">
                <x-image-component class="img-fluid"
                                   :picture="$slide['picture']"
                                   :src="asset('assets_polaris/img/default.png')">
                </x-image-component>
            </div>
        @endforeach
    </div>
</div>

@push('scripts')
    <script type="text/javascript" src="{{ asset('assets_polaris/js/flickity.pkgd.min.js')}}"></script>
@endpush
