<div class="d-flex flex-column flex-lg-row no-gutters border rounded bg-white o-hidden">

    @if (empty(optional($this->news)['url']))
        <div class="d-block position-relative bg-gradient col-xl-5">
            <x-image-component class="flex-fill hover-fade-out"
                               :picture="$this->news['picture']"
                               :src="asset('assets_polaris/img/default.png')">
            </x-image-component>
            <div class="divider divider-side bg-white d-none d-lg-block"></div>
        </div>
    @else
        <a href="{!! optional($this->news)['url'] !!}" class="d-block position-relative bg-gradient col-xl-5">
            <x-image-component class="flex-fill hover-fade-out"
                               :picture="$this->news['picture']"
                               :src="asset('assets_polaris/img/default.png')">
            </x-image-component>
            <div class="divider divider-side bg-white d-none d-lg-block"></div>
        </a>
    @endif

    <div class="p-4 p-md-5 col-xl-7 d-flex align-items-center">
        <div class="p-lg-4 p-xl-5">
            <div class="d-flex justify-content-between align-items-center mb-3 mb-xl-4">
                <div class="text-small text-muted">{!! optional($this->news)['date'] !!}</div>
            </div>
            @if (empty(optional($this->news)['url']))
                <h1>{!! optional($this->news)['title'] !!}</h1>
            @else
                <a href="{!! optional($this->news)['url'] !!}">
                    <h1>{!! optional($this->news)['title'] !!}</h1>
                </a>
            @endif

            <p class="lead">
                {!! optional($this->news)['short_description'] !!}
            </p>
            <div class="h6 mb-1">{!! optional($this->news)['author'] !!}</div>
            @if (!empty(optional($this->news)['url']))
                <a href="{!! optional($this->news)['url'] !!}" class="lead">Leggi tutto</a>
            @endif


        </div>
    </div>
</div>
