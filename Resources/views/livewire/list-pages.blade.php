<div class="row">
    @foreach($this->pages as $entity)
        <div class="col-md-6 col-lg-4 mb-3 mb-md-4 mb-lg-0">
            <div class="card h-100 hover-box-shadow">
                <div class="d-block bg-gradient rounded-top position-relative">
                    <x-image-component class="card-img-top hover-fade-out"
                                       :picture="$entity['picture']"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                </div>
                <div class="card-body">
                    <h3>{!! $entity['meta_title'] !!}</h3>
                    <p>{!! $entity['meta_description'] !!}</p>
                    <a href="{!! $entity['url'] !!}" class="stretched-link">Read Story</a>
                </div>
            </div>
        </div>
    @endforeach
</div>

