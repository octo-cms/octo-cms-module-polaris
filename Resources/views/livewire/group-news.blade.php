<div class="container">
    <div class="row">
        @foreach($this->news as $entity)
            @if($loop->first)

                <div class="col">
                    <div class="d-flex flex-column flex-lg-row no-gutters border rounded bg-white o-hidden">

                        @if (empty($entity['url']))
                            <div class="d-block position-relative bg-gradient col-xl-5">
                                <x-image-component class="flex-fill hover-fade-out"
                                                   :picture="$entity['picture']"
                                                   :src="asset('assets_polaris/img/default.png')">
                                </x-image-component>
                                <div class="divider divider-side bg-white d-none d-lg-block"></div>
                            </div>
                        @else
                            <a href="{!! $entity['url'] !!}" class="d-block position-relative bg-gradient col-xl-5">
                                <x-image-component class="flex-fill hover-fade-out"
                                                   :picture="$entity['picture']"
                                                   :src="asset('assets_polaris/img/default.png')">
                                </x-image-component>
                                <div class="divider divider-side bg-white d-none d-lg-block"></div>
                            </a>
                        @endif

                        <div class="p-4 p-md-5 col-xl-7 d-flex align-items-center">
                            <div class="p-lg-4 p-xl-5">
                                <div class="d-flex justify-content-between align-items-center mb-3 mb-xl-4">
                                    <div class="text-small text-muted">{!! $entity['date'] !!}</div>
                                </div>

                                @if (empty($entity['url']))
                                    <h1>{!! $entity['title'] !!}</h1>
                                @else
                                    <a href="{!! $entity['url'] !!}">
                                        <h1>{!! $entity['title'] !!}</h1>
                                    </a>
                                @endif

                                <p class="lead">
                                    {!! $entity['short_description'] !!}
                                </p>
                                <div class="h6 mb-1">{!! $entity['author'] !!}</div>
                                @if (!empty($entity['url']))
                                    <a href="{!! $entity['url'] !!}" class="lead">Leggi tutto</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        @endforeach
    </div>

    <div class="row mt-3 mt-lg-5">
        @foreach($this->news as $entity)
            @if(!$loop->first)
                <div class="col-lg-4 my-2 my-md-3 my-lg-0">
                    <div class="row">
                        <a class="col-5" href="#">
                            <x-image-component class="flex-fill hover-fade-out"
                                               :picture="$entity['picture']"
                                               :src="asset('assets_polaris/img/default.png')">
                            </x-image-component>
                        </a>
                        <div class="col">
                            @if (empty($entity['url']))
                                <p>{!! $entity['title'] !!}</p>
                            @else
                                <a class="h6" href="{!! $entity['url'] !!}">
                                    {!! $entity['title'] !!}
                                </a>
                            @endif

                            <div class="text-small text-muted mt-2">{!! $entity['date'] !!}</div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
