<div class="container">
    <div class="row section-title justify-content-center text-center">
        <div class="col-md-9 col-lg-8 col-xl-7">
            @if(!empty($pageLangData['title']))
                <h3 class="display-4">{!! $pageLangData['title'] !!}</h3>
            @endif
            @if(!empty($pageLangData['text']))
                <div class="lead">{!! $pageLangData['text'] !!}</div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="col-md-9 col-lg-4 col-xl-4 mt-4 mt-md-5 mb-4">
            <input wire:model="name" class="form-control" type="text" placeholder="Il tuo nome">
            @error('name') <span class="text-danger text-small">Devi inserire un nome</span> @enderror
        </div>
        <div class="col-md-9 col-lg-4 col-xl-4 mt-4 mt-md-5 mb-4">
            <input wire:model="email" class="form-control" type="text" placeholder="La tua email">
            @error('email') <span class="text-danger text-small">Devi inserire una email valida</span> @enderror
        </div>
    </div>
    @if($success && !empty($pageLangData['success-message']))
        <div class="row justify-content-center mt-4">
            <div class="col col-md-6 text-center">
                <div class="alert alert-success" role="alert">
                    {!! $pageLangData['success-message'] !!}
                </div>
            </div>
        </div>
    @endif

    <div class="row justify-content-center mt-4">
        <div class="col text-center">
            <div class="custom-control custom-checkbox">
                <input wire:model="privacy" type="checkbox" class="custom-control-input" id="get-started-agree-1">
                <label class="custom-control-label text-small @error('privacy') text-danger @enderror"
                       for="get-started-agree-1">
                    @if(!empty($pageLangData['privacy-test']))
                        {!! $pageLangData['privacy-test'] !!}
                    @else
                        I agree to the Terms & Conditions
                    @endif
                </label>
            </div>
            <button wire:click="saveRegistry" class="btn btn-primary flex-shrink-0 mt-3">
                @if(!empty($pageLangData['action']))
                    {!! $pageLangData['action'] !!}
                @endif
            </button>
        </div>
    </div>
</div>

