<div class="row">
    @foreach($this->news as $entity)
        <div class="col-md-6 col-lg-4 mb-3 mb-md-4 mb-lg-0">
            <div class="card h-100 hover-box-shadow">
                <div class="d-block bg-gradient rounded-top position-relative">
                    <x-image-component class="card-img-top hover-fade-out"
                                       :picture="$entity['picture']"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                </div>
                <div class="card-body text-dark">
                    <h3>{!! $entity['title'] !!}</h3>
                    <p>{!! $entity['short_description'] !!}</p>
                    <div class="h6 mb-1">{!! $entity['author'] !!}</div>
                    @if (!empty($entity['url']))
                        <a href="{!! $entity['url'] !!}" class="stretched-link">Leggi tutto</a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>

