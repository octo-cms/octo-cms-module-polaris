<div class="row text-center">
    @foreach($this->testimonials as $testimonial)
        <div class="col-md-4 mb-5 mb-lg-0">
            <x-image-component class="img-fluid"
                               :picture="$testimonial['picture']"
                               :src="asset('assets_polaris/img/default.png')">
            </x-image-component>
            &ldquo;{!! $testimonial['text'] !!}&rdquo;
            <div class="px-xl-4">
            </div>
        </div>
    @endforeach
</div>
