<section class="o-hidden
                {{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}
                {{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div class="row align-items-center justify-content-between text-center text-lg-left">
            <div class="col-md-9 col-lg-6 col-xl-5 mb-4 mb-md-5 mb-lg-0 pl-lg-5 pl-xl-0">
                <div>
                    @if(!empty($data['title']))
                        <h3 class="h1">{!! $data['title'] !!}</h3>
                    @endif
                    @if(!empty($data['text']))
                        <p class="lead">{!! $data['text'] !!}</p>
                    @endif
                    @if(!empty($data['link']) && !empty($data['action']))
                        <a href="{{$data['link']}}" class="lead">{!! $data['action'] !!}</a>
                    @endif
                </div>
            </div>
            <div class="col-md-9 col-lg-6 col-xl-5">
                @if(!empty($data['image']))
                    <x-image-component class="img-fluid"
                                       :picture="$data['image']"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                @endif
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
