<div class="col col-md-{{$size}}">
    @php($iubendaSettings = $getSettingByName(\OctoCmsModule\Core\Constants\SettingNameConst::FE_IUBENDA))
    @if(!empty($iubendaSettings) && $iubendaSettings['enabled'])
        <a href="{{$iubendaSettings['link']}}" target="_blank">Informativa privacy</a>
    @endif
</div>

