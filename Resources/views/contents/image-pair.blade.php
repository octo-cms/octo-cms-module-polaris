<section class="{{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}
                {{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-9 col-lg-6 col-xl-5">
                @if(!empty($data['image-left']))
                    <x-image-component class="img-fluid"
                                       :picture="$data['image-left']"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                @endif
            </div>
            <div class="col-md-9 col-lg-6 col-xl-5">
                @if(!empty($data['image-right']))
                    <x-image-component class="img-fluid"
                                       :picture="$data['image-right']"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                @endif
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
