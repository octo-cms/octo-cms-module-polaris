<div data-overlay
     class="jarallax {{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-primary-3 text-white'}}"
     data-jarallax data-speed="0.2">
    @if(!empty($data['image']))
        <x-image-component class="jarallax-img opacity-30"
                           :picture="$data['image']"
                           :src="asset('assets_polaris/img/default.png')">
        </x-image-component>
    @endif
    <section class="{{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                    {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
        @if(!empty($layout['divider-top']))
            <div
                class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
            </div>
        @endif
        <div class="container pb-5">
            <div class="row justify-content-center text-center">
                <div class="col-lg-10 col-md-11">
                    @if(!empty($data['video']))
                        <a data-fancybox href="{!! $data['video'] !!}"
                           class="btn btn-lg btn-light rounded-circle mb-4 mb-md-5" data-aos="zoom-in"
                           data-aos-delay="400">
                            <img data-src="{{asset('assets_polaris/img/icons/interface/icon-media-play.svg')}}"
                                 alt="Media Play Icon" class="icon icon-lg pl-1">
                        </a>
                    @endif
                    @if(!empty($data['title']))
                        <h1 class="display-3" data-aos="fade-up" data-aos-delay="100">{!! $data['title'] !!}</h1>
                    @endif
                    @if(!empty($data['text']))
                        <p class="lead" data-aos="fade-up" data-aos-delay="200">
                            {!! $data['text'] !!}
                        </p>
                    @endif
                    @if(!empty($data['action']) && !empty($data['target']) && !empty($data['label']))
                        <div class="d-flex flex-column flex-sm-row justify-content-center mt-4 mt-md-5"
                             data-aos="fade-up" data-aos-delay="300">
                            <a href="{!! $data['action'] !!}" target="{!! $data['target'] !!}"
                               class="btn btn-primary btn-lg mx-sm-2 my-1 my-sm-0">{!! $data['label'] !!}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if(!empty($layout['divider-bottom']))
            <div
                class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
            </div>
        @endif
    </section>
</div>
