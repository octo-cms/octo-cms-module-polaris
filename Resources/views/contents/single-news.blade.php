<section class="{{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-light'}}
                {{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col">
                @livewire('single-news', ['targets' => $targets])
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
