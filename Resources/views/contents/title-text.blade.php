<section class="{{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}
                {{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-9 col-lg-8 col-xl-7">
                @if(!empty($data['text']))
                    <div class="lead">{!! $data['text'] !!}</div>
                @endif
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
