<div class="col col-md-{{$size}} d-flex justify-content-center">
    @php($socialSharingSettings = $getSettingByName(\OctoCmsModule\Core\Constants\SettingNameConst::FE_SOCIAL_SHARING))
    @if(!empty($socialSharingSettings))
        @if(!empty($socialSharingSettings['facebook']) && $socialSharingSettings['facebook']['enabled'])
            <a class="mx-2 hover-fade-out" href="{{$socialSharingSettings['facebook']['link'] ?: '#'}}" target="_blank">
                <img src="{{asset('assets_polaris/img/icons/social/facebook.svg')}}"
                     alt="Facebook"
                     class="icon icon-xs bg-white"
                     data-inject-svg>
            </a>
        @endif
        @if(!empty($socialSharingSettings['twitter']) && $socialSharingSettings['twitter']['enabled'])
            <a class="mx-2 hover-fade-out" href="{{$socialSharingSettings['twitter']['link'] ?: '#'}}" target="_blank">
                <img src="{{asset('assets_polaris/img/icons/social/twitter.svg')}}"
                     alt="Twitter"
                     class="icon icon-xs bg-white"
                     data-inject-svg>
            </a>
        @endif
        @if(!empty($socialSharingSettings['instagram']) && $socialSharingSettings['instagram']['enabled'])
            <a class="mx-2 hover-fade-out" href="{{$socialSharingSettings['instagram']['link'] ?: '#'}}"
               target="_blank">
                <img src="{{asset('assets_polaris/img/icons/social/instagram.svg')}}"
                     alt="Instagram"
                     class="icon icon-xs bg-white"
                     data-inject-svg>
            </a>
        @endif
    @endif
</div>
