<section class="o-hidden
                {{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}
{{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
{{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div
            class="row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left flex-lg-row-reverse">
            <div class="col-md-9 col-lg-6 col-xl-5 mb-4 mb-lg-0 pr-lg-5 pr-xl-0">
                <div data-aos="fade-in" data-aos-delay="250">
                    @if(!empty($data['title']))
                        <h1 class="display-3">{!! $data['title'] !!}</h1>
                    @endif
                    @if(!empty($data['subtitle']))
                        <p class="lead">{!! $data['subtitle'] !!}</p>
                    @endif
                    <div class="mt-4 mt-md-5">
                        <form class="d-flex flex-column">
                            <input class="form-control h-100" type="text" name="get-started-name"
                                   placeholder="Your Name">
                            <input class="form-control h-100 my-3" type="email" name="get-started-email"
                                   placeholder="Email Address">
                            <button class="btn btn-primary flex-shrink-0" type="submit">Create your account</button>
                            <div class="custom-control custom-checkbox mt-3">
                                <input type="checkbox" class="custom-control-input" id="get-started-agree-1">
                                <label class="custom-control-label text-small" for="get-started-agree-1">I agree to the
                                    Terms & Conditions</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-6" data-aos="fade-left">
                <x-image-component class="img-fluid"
                                   :picture="$data['image']"
                                   :src="asset('assets_polaris/img/default.png')">
                </x-image-component>
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
