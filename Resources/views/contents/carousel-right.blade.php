<section class="p-0 row no-gutters {{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}">
    <div class="col-lg-7 col-xl-6">
        <div class="container min-vh-lg-80 d-flex align-items-center">
            <div class="row justify-content-center flex-grow-1">
                <div class="col col-md-10 col-xl-9 text-center text-lg-left">
                    <section>
                        <div data-aos="fade-right">
                            @if(!empty($data['title']))
                                <h1 class="display-3">
                                    <mark data-aos="highlight-text" data-aos-delay="200">{!! $data['title'] !!}</mark>
                                </h1>
                            @endif
                            @if(!empty($data['text']))
                                <p class="lead">{!! $data['text'] !!}</p>
                            @endif
                        </div>
                        <div
                            class="d-flex flex-column flex-sm-row mt-4 mt-md-5 justify-content-center justify-content-lg-start"
                            data-aos="fade-right" data-aos-delay="300">
                            @if(!empty($data['action']) && !empty($data['target']) && !empty($data['label']))
                                <a href="{{$data['action']}}" target="{{$data['target']}}"
                                   class="btn btn-primary btn-lg mx-sm-2 mx-lg-0 mr-lg-2 my-1 my-sm-0">
                                    {!! $data['label'] !!}
                                </a>
                            @endif
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5 col-xl-6 d-lg-flex flex-lg-column">
        <div class="divider divider-side transform-flip-y d-none d-lg-block {{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}"></div>
        <div class="d-lg-flex flex-column flex-fill controls-hover"
             data-flickity='{ "imagesLoaded": true, "wrapAround":true, "pageDots":false, "autoPlay":true }'>
            <div class="carousel-cell text-center">
                <x-image-component class="img-fluid"
                                   :picture="$data['image-1']"
                                   :src="asset('assets_polaris/img/default.png')">
                </x-image-component>
            </div>
            <div class="carousel-cell text-center">
                <x-image-component class="img-fluid"
                                   :picture="$data['image-2']"
                                   :src="asset('assets_polaris/img/default.png')">
                </x-image-component>
            </div>
        </div>
    </div>
</section>

@push('scripts')
    <script type="text/javascript" src="{{ asset('assets_polaris/js/flickity.pkgd.min.js')}}"></script>
@endpush
