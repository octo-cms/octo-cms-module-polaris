<section class="o-hidden
                {{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-light'}}
                {{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-top-bg-color']) ? $layout['divider-top-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div class="row section-title justify-content-center text-center">
            <div class="col-md-9 col-lg-8 col-xl-7">
                <h2 class="display-4">{!! $data['title'] !!}</h2>
                <div class="lead">{!! $data['subtitle'] !!}</div>
            </div>
        </div>
        <div class="row align-items-center justify-content-between">
            <div class="col-md-9 col-lg-5" data-aos="fade-in">
                @if(!empty($data['image-1']))
                    <x-image-component class="img-fluid rounded shadow"
                                       :picture="$data['image-1']"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                @endif
                @if(!empty($data['image-2']))
                    <x-image-component
                        class="lazy position-absolute p-0 col-4 col-xl-5 border border-white border-thick rounded-circle top left shadow-lg mt-5 ml-n5 ml-lg-n3 ml-xl-n5 d-none d-md-block"
                        :picture="$data['image-2']"
                        :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                @endif
            </div>
            <div class="col-md-9 col-lg-6 col-xl-5 mt-4 mt-md-5 mt-lg-0">
                <ol class="list-unstyled p-0">
                    @if(!empty($data['image-bullet-list']))
                        @foreach($data['image-bullet-list'] as $bulletListElement)
                            <li class="d-flex align-items-start my-4 my-md-5">
                                <div
                                    class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center bg-success">
                                    <div class="position-absolute text-white h5 mb-0">{!! $loop->iteration !!}</div>
                                </div>
                                <div class="ml-3 ml-md-4">
                                    @if(!empty($bulletListElement['title']))
                                        <h4>{!! $bulletListElement['title'] !!}</h4>
                                    @endif
                                    @if(!empty($bulletListElement['subtitle']))
                                        <p>{!! $bulletListElement['subtitle'] !!}</p>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ol>
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
