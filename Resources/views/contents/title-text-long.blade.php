<section class="{{!empty($layout['bg-color']) ? $layout['bg-color'] : 'bg-white'}}
                {{!empty($layout['padding-top']) ? $layout['padding-top'] : ''}}
                {{!empty($layout['padding-bottom']) ? $layout['padding-bottom'] : ''}}">
    @if(!empty($layout['divider-top']))
        <div
            class="divider divider-top transform-flip-x {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                @if(!empty($data['title']))
                    <h1 class="display-4">{!! $data['title'] !!}</h1>
                @endif
                @if(!empty($data['text']))
                        <div class="lead">{!! $data['text'] !!}</div>
                @endif
            </div>
        </div>
    </div>
    @if(!empty($layout['divider-bottom']))
        <div
            class="divider divider-bottom {{!empty($layout['divider-bottom-bg-color']) ? $layout['divider-bottom-bg-color'] : 'bg-white'}}">
        </div>
    @endif
</section>
