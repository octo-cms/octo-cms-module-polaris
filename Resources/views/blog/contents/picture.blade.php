<figure>
    @if(!empty($picture))
        <x-image-component class="img-fluid rounded"
                           :picture="$picture->toArray()"
                           :src="asset('assets_polaris/img/default.png')">
        </x-image-component>
        <figcaption>Here’s a great little figure caption.</figcaption>
    @endif
</figure>
