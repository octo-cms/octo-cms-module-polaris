<section class="p-0 border-top border-bottom bg-light row no-gutters">
    <div class="col-lg-5 col-xl-6 order-lg-2">
        <div class="divider divider-side transform-flip-y bg-light d-none d-lg-block"></div>
        @php($mainImage = $news->pictures->where('tag', '=', 'main')->first())
        @if(!empty($mainImage))
            <x-image-component class="flex-fill"
                               :picture="$mainImage->toArray()"
                               :src="asset('assets_polaris/img/default.png')">
            </x-image-component>
        @endif
    </div>
    <div class="col-lg-7 col-xl-6">
        <div class="container min-vh-lg-70 d-flex align-items-center">
            <div class="row justify-content-center">
                <div class="col col-md-10 col-xl-9 py-4 py-sm-5">
                    <div class="my-4 my-md-5">
                        <div class="d-flex align-items-center mb-3 mb-xl-4">
                            <div class="ml-3 text-small text-muted">
                                {{optional($news->date)->format('d/m/Y') }}
                            </div>
                        </div>
                        <h1 class="display-4">
                            {{$getLangValue($news->newsLangs, 'title')}}
                        </h1>
                        <a href="#" class="d-flex align-items-center">
                            @php($authorImage = $news->pictures->where('tag', '=', 'author')->first())
                            @if(!empty($authorImage))
                                <x-image-component class="avatar avatar-sm"
                                                   :picture="$authorImage->load('pictureLangs')->toArray()"
                                                   :src="asset('assets_polaris/img/default.png')">
                                </x-image-component>
                            @endif
                            <div class="h6 mb-0 ml-3">
                                {{$news->author}}
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@if(!empty($news->newsContents))
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-10">
                    <article class="article">
                        @foreach($news->newsContents as $newsContent)
                            <x-news-content-handler :news-content="$newsContent"/>
                        @endforeach
                    </article>
                </div>
            </div>
        </div>
    </section>
@endif
