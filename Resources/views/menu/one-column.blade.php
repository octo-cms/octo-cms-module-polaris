@if(!empty($data))
<div class="col-xl-auto mr-xl-5 col-md-3 mb-4 mb-md-0">
    <h5>{{   $data['title']}}</h5>
    @if(!empty($data['items']))
        <ul class="nav flex-row flex-md-column">
            @foreach($data['items'] as $item)
            <li class="nav-item mr-3 mr-md-0"><a href="{{$item->link}}" class="nav-link fade-page px-0 py-2">{{$item->label}}</a></li>
            @endforeach
        </ul>
    @endif
</div>
@endif


