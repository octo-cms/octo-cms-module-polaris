<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    @if(!empty($optimizeSettings['enabled']))
        @include('sitebuilder::partials.optimize-anti-flicker', ['container_id' => $optimizeSettings['container_id']])
        @include('sitebuilder::partials.ga-tag', ['container_id' => $optimizeSettings['container_id'], 'property_id' => $optimizeSettings['property_id']])
    @endif
    @if(!empty($gtmSettings['enabled']))
        @include('sitebuilder::partials.gtm-head', ['container_id' => $gtmSettings['container_id']])
    @endif
    <meta charset="utf-8">
    <title>{{$metaTitle}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$metaDescription}}">

    @if(!empty($faviconsData))
            @include('sitebuilder::partials.favicon', ['faviconsData' => $faviconsData])
    @endif

    <link href="{{ mix('assets_polaris/css/main.min.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,700&display=swap" rel="stylesheet">

    @stack('css')

    @livewireStyles

</head>
<body>
@if(!empty($gtmSettings['enabled']))
    @include('sitebuilder::partials.gtm-body', ['container_id' => $gtmSettings['container_id']])
@endif
@include('polaris::partials.header')

@yield('content')

@include('polaris::partials.footer')

<script type="text/javascript" src="{{ mix('assets_polaris/js/all-scripts.min.js')}}"></script>

@include('sitebuilder::partials.lazyload')

@stack('scripts')

@livewireScripts
</body>
</html>
