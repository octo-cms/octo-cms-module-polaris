@inject('imageUtils', 'OctoCmsModule\Sitebuilder\Utils\ImageUtils')

<div class="navbar-container">
    <nav class="navbar navbar-expand-lg navbar-light shadow" data-sticky="top">
        <div class="container">

            <!-- Brand Logo -->
            <a class="navbar-brand navbar-brand-dynamic-color fade-page" href="/">
                @if(!empty($logo))
                    <x-image-component class="logo-header"
                                       :picture="$logo"
                                       :src="asset('assets_polaris/img/default.png')">
                    </x-image-component>
                @endif
            </a>

            <livewire:main-menu/>

        </div>
        <!-- End container -->

    </nav>
    <!-- End nav -->

</div>
<!-- End navbar container-->

