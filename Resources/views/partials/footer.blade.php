<div class="bg-primary-3 text-white links-white py-4 footer-1">
    @if(!empty($footers))
        @foreach($footers as $footer)
            <div class="container">
                <div class="row {{$footer['content_align']}}">
                    @foreach($footer['contents'] as $content)
                        <x-footer-content-handler :content="$content"/>
                    @endforeach
                </div>
            </div>
        @endforeach
    @endif
</div>
