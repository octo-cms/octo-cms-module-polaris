<?php

namespace OctoCmsModule\Polaris\Console;

/**
 * Class PolarisFooterBlocks
 *
 * @package OctoCmsModule\Polaris\Console
 */
class PolarisFooterBlocks extends PolarisLayoutValues
{
    public const TYPE_HTML = 'html';
    public const TYPE_ENTITY = 'entity';

    public const BLOCKS = [
        [
            'type'         => self::TYPE_HTML,
            'blade'        => 'copyright',
            'instructions' => 'Sezione con il copyright',
            'layout'       => [self::LAYOUT_TEXT_ALIGN],
        ],
        [
            'type'         => self::TYPE_HTML,
            'blade'        => 'iubenda',
            'instructions' => 'Area per link iubenda',
            'layout'       => [self::LAYOUT_TEXT_ALIGN],
        ],
        [
            'type'         => self::TYPE_HTML,
            'blade'        => 'social-sharing',
            'instructions' => 'Area di social sharing'
        ],
    ];
}
