<?php

namespace OctoCmsModule\Polaris\Console;

/**
 * Class PolarisLayoutValues
 *
 * @package OctoCmsModule\Polaris\Console
 */
class PolarisLayoutValues
{
    public const TYPE_SELECT = 'select';
    public const TYPE_BOOLEAN = 'boolean';

    public const INSTRUCTIONS_BOOLEAN = 'Scegli se visualizzare o meno questo elemento.';
    public const INSTRUCTIONS_SELECT_PADDING = 'Seleziona una classe per impostare il valore del padding.';
    public const INSTRUCTIONS_SELECT_TEXT_ALIGN = "Seleziona l'allineamento dei contenuti.";
    public const INSTRUCTIONS_COL_SIZE = "Seleziona la larghezza della colonna";
    public const INSTRUCTIONS_SELECT_BG = 'Seleziona una classe per impostare il colore dello sfondo
    di questo elemento.';

    public const COL_SIZE_VALUES = ['4', '6', '8', '10', '12'];
    public const PT_VALUES = ['pt-0', 'pt-1', 'pt-2', 'pt-3', 'pt-4'];
    public const PB_VALUES = ['pb-0', 'pb-1', 'pb-2', 'pb-3', 'pb-4'];
    public const TEXT_ALIGNMENTS_OPTIONS = ['text-left', 'text-center', 'text-right'];
    public const BG_COLORS = [
        'bg-light',
        'bg-dark text-white',
        'bg-white',
        'bg-transparent',
        'bg-danger text-white',
        'bg-warning',
        'bg-info',
        'bg-success',
        'bg-primary',
        'bg-primary-2 text-white',
        'bg-primary-3 text-white',
    ];

    public const LAYOUT_BG_COLOR = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'bg-color',
        'options'      => self::BG_COLORS,
        'instructions' => self::INSTRUCTIONS_SELECT_BG,
    ];

    public const LAYOUT_PADDING_TOP = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'padding-top',
        'options'      => self::PT_VALUES,
        'instructions' => self::INSTRUCTIONS_SELECT_PADDING,
    ];

    public const LAYOUT_PADDING_BOTTOM = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'padding-bottom',
        'options'      => self::PB_VALUES,
        'instructions' => self::INSTRUCTIONS_SELECT_PADDING,
    ];

    public const LAYOUT_DIVIDER_TOP = [
        'type'         => self::TYPE_BOOLEAN,
        'name'         => 'divider-top',
        'instructions' => self::INSTRUCTIONS_BOOLEAN,
    ];

    public const LAYOUT_DIVIDER_TOP_BG_COLOR = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'divider-top-bg-color',
        'options'      => self::BG_COLORS,
        'instructions' => self::INSTRUCTIONS_SELECT_BG,
    ];

    public const LAYOUT_DIVIDER_BOTTOM = [
        'type'         => self::TYPE_BOOLEAN,
        'name'         => 'divider-bottom',
        'instructions' => self::INSTRUCTIONS_BOOLEAN,
    ];

    public const LAYOUT_DIVIDER_BOTTOM_BG_COLOR = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'divider-bottom-bg-color',
        'options'      => self::BG_COLORS,
        'instructions' => self::INSTRUCTIONS_SELECT_BG,
    ];

    public const LAYOUT_COL_LG_SIZE = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'col-lg-size',
        'options'      => self::COL_SIZE_VALUES,
        'instructions' => self::INSTRUCTIONS_COL_SIZE,
    ];

    public const LAYOUT_COL_XL_SIZE = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'col-xl-size',
        'options'      => self::COL_SIZE_VALUES,
        'instructions' => self::INSTRUCTIONS_COL_SIZE,
    ];

    public const LAYOUT_TEXT_ALIGN = [
        'type'         => self::TYPE_SELECT,
        'name'         => 'text-align',
        'options'      => self::TEXT_ALIGNMENTS_OPTIONS,
        'instructions' => self::INSTRUCTIONS_SELECT_TEXT_ALIGN,
    ];

    public const LAYOUT = [
        self::LAYOUT_BG_COLOR,
        self::LAYOUT_PADDING_TOP,
        self::LAYOUT_PADDING_BOTTOM,
        self::LAYOUT_DIVIDER_TOP,
        self::LAYOUT_DIVIDER_TOP_BG_COLOR,
        self::LAYOUT_DIVIDER_BOTTOM,
        self::LAYOUT_DIVIDER_BOTTOM_BG_COLOR,
    ];
}
