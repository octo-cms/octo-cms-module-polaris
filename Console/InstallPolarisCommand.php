<?php

namespace OctoCmsModule\Polaris\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;

/**
 * Class InstallPolarisCommand
 *
 * @package OctoCmsModule\Polaris\Console
 */
class InstallPolarisCommand extends Command
{
    protected const MODULE_NAME = 'Polaris';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:polaris';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Running Install Polaris Command ...');

        $this->info('Creating BlockHtml ...');

        foreach (PolarisHtmlBlocks::BLOCKS as $htmlBlock) {
            /** @var string $blade */
            $blade = Arr::get($htmlBlock, 'blade', '');

            BlockHtml::updateOrCreate(
                [
                    'blade'  => $blade,
                    'module' => self::MODULE_NAME,
                ],
                [
                    'target'       => BlockHtml::TARGET_PAGE,
                    'settings'     => Arr::get($htmlBlock, 'values', ''),
                    'instructions' => Arr::get($htmlBlock, 'instructions', ''),
                    'layout'       => Arr::get($htmlBlock, 'layout', ''),
                    'src'          => Arr::get($htmlBlock, 'src', ''),
                ]
            );
        }

        $this->info('Creating BlockEntity ...');

        foreach (PolarisEntityBlocks::BLOCKS as $entityBlock) {
            /** @var string $blade */
            $blade = Arr::get($entityBlock, 'blade', '');

            BlockEntity::updateOrCreate(
                [
                    'blade'  => $blade,
                    'module' => self::MODULE_NAME,
                    'entity' => Arr::get($entityBlock, 'entity', ''),
                ],
                [
                    'target'       => BlockEntity::TARGET_PAGE,
                    'settings'     => Arr::get($entityBlock, 'values', []),
                    'instructions' => Arr::get($entityBlock, 'instructions', ''),
                    'layout'       => Arr::get($entityBlock, 'layout', ''),
                    'src'          => Arr::get($entityBlock, 'src', ''),
                ]
            );
        }


        $this->info('Creating Footer Blocks ...');

        foreach (PolarisFooterBlocks::BLOCKS as $block) {
            BlockHtml::updateOrCreate(
                [
                    'blade'  => Arr::get($block, 'blade', ''),
                    'module' => self::MODULE_NAME,
                ],
                [
                    'target'       => BlockHtml::TARGET_FOOTER,
                    'instructions' => Arr::get($block, 'instructions', ''),
                    'layout'       => Arr::get($block, 'layout', ''),
                ]
            );
        }
    }
}
