<?php

namespace OctoCmsModule\Polaris\Console;

/**
 * Class PolarisHtmlBlocks
 *
 * @package OctoCmsModule\Polaris\Console
 */
class PolarisHtmlBlocks extends PolarisLayoutValues
{
    public const TYPE_STRING       = 'string';
    public const TYPE_IMAGE        = 'image';
    public const TYPE_HTML         = 'html';
    public const TYPE_BULLET_LIST  = 'bulletList';
    public const TYPE_COLOR_PICKER = 'colorPicker';

    public const BLOCKS = [
        [
            'blade'        => 'header-subscribe',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'subtitle', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/header-subscribe.png'
        ],
        [
            'blade'        => 'header-video',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'video', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'action', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'target', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'label', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/header-video.png'
        ],
        [
            'blade'        => 'title-text',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_HTML, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/title-text.png'
        ],
        [
            'blade'        => 'image-list',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'subtitle', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image-1', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image-2', 'instructions' => 'qua mancano le istruzioni'],
                [
                    'type'         => self::TYPE_BULLET_LIST,
                    'name'         => 'image-bullet-list',
                    'instructions' => 'qua mancano le istruzioni',
                ],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/image-list.png'
        ],
        [
            'blade'        => 'carousel-right',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'action', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'target', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'label', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image-1', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image-2', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => [self::LAYOUT_BG_COLOR],
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/carousel-right.png'
        ],
        [
            'blade'        => 'image-text-left',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'target', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'action', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'link', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/image-text-left.png'
        ],
        [
            'blade'        => 'email-form',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'action', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'privacy-test', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'success-message', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/email-form.png'
        ],
        [
            'blade'        => 'image-text-right',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'target', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'action', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_STRING, 'name' => 'link', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/image-text-right.png'
        ],
        [
            'blade'        => 'title-text-long',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_HTML, 'name' => 'text', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/title-text-long.png'
        ],
        [
            'blade'        => 'single-image',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => [
                self::LAYOUT_BG_COLOR,
                self::LAYOUT_PADDING_TOP,
                self::LAYOUT_PADDING_BOTTOM,
                self::LAYOUT_DIVIDER_TOP,
                self::LAYOUT_DIVIDER_TOP_BG_COLOR,
                self::LAYOUT_DIVIDER_BOTTOM,
                self::LAYOUT_DIVIDER_BOTTOM_BG_COLOR,
                self::LAYOUT_COL_LG_SIZE,
                self::LAYOUT_COL_XL_SIZE
            ],
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/single-image.png'
        ],
        [
            'blade'        => 'image-pair',
            'instructions' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus lectus.',
            'values'       => [
                ['type' => self::TYPE_IMAGE, 'name' => 'image-left', 'instructions' => 'qua mancano le istruzioni'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image-right', 'instructions' => 'qua mancano le istruzioni'],
            ],
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/image-pair.png'
        ],
    ];
}
