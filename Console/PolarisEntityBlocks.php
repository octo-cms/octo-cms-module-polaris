<?php

namespace OctoCmsModule\Polaris\Console;

/**
 * Class PolarisEntityBlocks
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class PolarisEntityBlocks extends PolarisLayoutValues
{
    public const BLOCKS = [
        [
            'blade'        => 'slider',
            'values'       => [],
            'entity'       => 'Slide',
            'instructions' => 'Aggiungi slides.',
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/slider.png'
        ],
        [
            'blade'        => 'testimonials',
            'values'       => [],
            'entity'       => 'Testimonial',
            'instructions' => 'Aggiungi massimo 3 testimonial. Se aggiungi di più, gli ultimi
            non verranno visualizzati.',
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/testimonials.png'
        ],
        [
            'blade'        => 'list-news',
            'values'       => [],
            'entity'       => 'News',
            'instructions' => 'Aggiungi massimo 3 news. Se aggiungi di più, le ultime non verranno visualizzati.',
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/list-news.png'
        ],
        [
            'blade'        => 'single-news',
            'values'       => [],
            'entity'       => 'News',
            'instructions' => 'Aggiungi una (1) news.',
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/single-news.png'
        ],
        [
            'blade'        => 'group-news',
            'values'       => [],
            'entity'       => 'News',
            'instructions' => 'Aggiungi 4 news. La prima sarà la news principale.',
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/group-news.png'
        ],
        [
            'blade'        => 'list-pages',
            'values'       => [],
            'entity'       => 'Page',
            'instructions' => 'Aggiungi massimo 3 pages. Se aggiungi di più, le ultime non verranno visualizzati.',
            'layout'       => self::LAYOUT,
            'src'          => 'https://storage.googleapis.com/octo-cms-dist/Polaris/list-pages.png'
        ],
    ];
}
