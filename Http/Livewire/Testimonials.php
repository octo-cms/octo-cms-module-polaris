<?php

namespace OctoCmsModule\Polaris\Http\Livewire;

use Livewire\Component;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;
use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Class Testimonials
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Http\Livewire
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class Testimonials extends Component
{
    public const LIMIT = 3;

    public $targets = [];

    /**
     * LivewireUtils
     *
     * @var LivewireUtils
     */
    protected $livewireUtils;

    /**
     * Name mount
     *
     * @param LivewireUtils $livewireUtils LivewireUtils
     * @param array         $targets       array
     *
     * @return void
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * Name getTestimonialsProperty
     *
     * @return array
     */
    public function getTestimonialsProperty()
    {

        /**
         * Testimonials
         *
         * @var Testimonial[] $testimonials
         */
        $entities = $this->livewireUtils->getEntities(
            Testimonial::with('testimonialLangs'),
            $this->targets
        );


        if (empty($entities)) {
            return [];
        }

        $testimonialsToPublish = [];

        $count = 0;

        foreach ($entities as $entity) {
            $picture = $entity->getPictures()->first();

            if ($count >= self::LIMIT) {
                break;
            }

            $testimonialsToPublish[] = [
                'id'      => $entity->id,
                'author'  => $entity->author,
                'job'     => LanguageUtils::getLangValue($entity->testimonialLangs, 'job'),
                'text'    => LanguageUtils::getLangValue($entity->testimonialLangs, 'text'),
                'picture' => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
            ];

            $count++;
        }

        return $testimonialsToPublish;
    }


    /**
     * Name render
     *
     * @return mixed
     */
    public function render()
    {
        return view()->first(
            [
                'livewire.testimonials',
                'polaris::livewire.testimonials',
            ]
        );
    }
}
