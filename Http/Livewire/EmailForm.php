<?php

namespace OctoCmsModule\Polaris\Http\Livewire;

use Livewire\Component;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\PrivateRegistry;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Interfaces\RegistryServiceInterface;
use OctoCmsModule\Core\Jobs\CreateActiveCampaignJob;

/**
 * Class EmailForm
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Http\Livewire
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class EmailForm extends Component
{
    /**
     * Name
     *
     * @var string
     */
    public $name;

    /**
     * Email
     *
     * @var string
     */
    public $email;

    /**
     * Privacy
     *
     * @var bool
     */
    public $privacy;

    /**
     * PageLangData
     *
     * @var array
     */
    public $pageLangData;

    /**
     * Message
     *
     * @var string
     */
    public $message;

    /**
     * Success
     *
     * @var bool
     */
    public $success;

    /**
     * Rules
     *
     * @var string[]
     */
    protected $rules = [
        'name'    => 'required|string|min:3',
        'email'   => 'required|email',
        'privacy' => 'accepted',
    ];

    /**
     * Name mount
     *
     * @param array $pageLangData Page Lang Data
     *
     * @return void
     */
    public function mount($pageLangData = [])
    {
        $this->pageLangData = $pageLangData;
        $this->success = false;
    }

    /**
     * Name saveRegistry
     *
     * @param RegistryServiceInterface $registryService RegistryServiceInterface
     *
     * @return void
     */
    public function saveRegistry(RegistryServiceInterface $registryService)
    {
        $this->validate();

        $registry = new Registry(['type' => Registry::TYPE_PRIVATE]);

        $registry->save();

        $privateRegistry = new PrivateRegistry(
            [ 'name'    => $this->name ]
        );

        $privateRegistry->registry()->associate($registry);

        $privateRegistry->save();

        $email = new Email(
            [
            'email'   => $this->email,
            'default' => true,
            'label'   => 'default'
            ]
        );

        $registry->emails()->save($email);

        $email->save();

        $this->success = true;

        $contactFormDataDTO = new ContactFormDataDTO();
        $contactFormDataDTO->email = $this->email;
        $contactFormDataDTO->firstName = $this->name;
        $contactFormDataDTO->lastName = '';
        $contactFormDataDTO->phone = '';
        $contactFormDataDTO->message = $this->message;

        $registryService->sendEmailToAdmin($contactFormDataDTO);

        CreateActiveCampaignJob::dispatch($contactFormDataDTO);
    }

    /**
     * Name render
     *
     * @return mixed
     */
    public function render()
    {
        return view()->first(
            [
            'livewire.email-form',
            'polaris::livewire.email-form',
            ]
        );
    }
}
