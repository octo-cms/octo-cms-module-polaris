<?php

namespace OctoCmsModule\Polaris\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

/**
 * Class Slider
 *
 * @package OctoCmsModule\Polaris\Http\Livewire
 */
class Slider extends Component
{
    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array         $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return array
     */
    public function getSlidesProperty()
    {
        /** @var Slide[] $entities */
        $entities = $this->livewireUtils->getEntities(
            Slide::with('pictures'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        $slidesToPublish = [];

        foreach ($entities as $entity) {
            $picture = $entity->getPictures()->first();

            $slidesToPublish[] = [
                'id'           => $entity->id,
                'caption'      => $entity->caption,
                'sub_caption'  => $entity->sub_caption,
                'action_link'  => $entity->action_link,
                'action_label' => $entity->action_label,
                'picture'      => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
            ];
        }

        return $slidesToPublish;
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.slider',
            'polaris::livewire.slider',
        ]);
    }
}
