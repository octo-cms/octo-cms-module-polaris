<?php

declare(strict_types=1);

namespace OctoCmsModule\Polaris\Http\Livewire;

use Livewire\Component;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

use function trim;
use function view;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Http\Livewire
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 */
class ListNews extends Component
{
    public const LIMIT = 3;

    public $targets = [];

    protected $livewireUtils;

    /**
     * Name mount
     *
     * @param LivewireUtils $livewireUtils LivewireUtils
     * @param array         $targets       array
     */
    public function mount(LivewireUtils $livewireUtils, array $targets = []): void
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * Name getNewsProperty
     *
     * @return array
     */
    public function getNewsProperty(): array
    {
        /**
         * News
         *
         * @var News[] $news
         */
        $entities = $this->livewireUtils->getEntities(
            News::with('newsLangs')
                ->with('pictures')
                ->with('categories')
                ->with('page')
                ->with('page.pageLangs'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        $newsToPublish = [];

        $count = 0;

        foreach ($entities as $entity) {
            $picture = $entity->getPictures()->first();

            if ($count >= self::LIMIT) {
                break;
            }

            $url = '';
            if (!empty($entity->page->pageLangs)) {
                $url = trim(LanguageUtils::getLangValue($entity->page->pageLangs, 'url'));
            }

            $newsToPublish[] = [
                'id'                => $entity->id,
                'title'             => LanguageUtils::getLangValue($entity->newsLangs, 'title'),
                'date'              => $entity->date->format('d/m/Y'),
                'short_description' => LanguageUtils::getLangValue($entity->newsLangs, 'short_description'),
                'picture'           => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
                'author'            => $entity->author,
                'url'               => $url,
            ];

            $count++;
        }

        return $newsToPublish;
    }

    /**
     * Name render
     *
     * @return mixed
     */
    public function render()
    {
        return view()->first(
            [
                'livewire.list-news',
                'polaris::livewire.list-news',
            ]
        );
    }
}
