<?php

namespace OctoCmsModule\Polaris\Http\Livewire;

use Livewire\Component;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

/**
 * Class ListPages
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Http\Livewire
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ListPages extends Component
{
    public const LIMIT = 3;

    public $targets = [];

    protected $livewireUtils;

    /**
     * Name mount
     *
     * @param LivewireUtils $livewireUtils LivewireUtils
     * @param array         $targets       array
     *
     * @return void
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * Name getPagesProperty
     *
     * @return array
     */
    public function getPagesProperty()
    {

        /**
         * Pages
         *
         * @var Page[] $pages
         */
        $entities = $this->livewireUtils->getEntities(
            Page::with('pageLangs')
                ->with('pictures'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        $pagesToPublish = [];

        $count = 0;

        foreach ($entities as $entity) {
            $picture = $entity->getPictures()->first();

            if ($count >= self::LIMIT) {
                break;
            }

            $pagesToPublish[] = [
                'id'               => $entity->id,
                'meta_title'       => LanguageUtils::getLangValue($entity->pageLangs, 'meta_title'),
                'meta_description' => LanguageUtils::getLangValue($entity->pageLangs, 'meta_description'),
                'url'              => LanguageUtils::getLangValue($entity->pageLangs, 'url'),
                'picture'          => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
            ];

            $count++;
        }

        return $pagesToPublish;
    }


    /**
     * Name render
     *
     * @return mixed
     */
    public function render()
    {
        return view()->first(
            [
                'livewire.list-pages',
                'polaris::livewire.list-pages',
            ]
        );
    }
}
