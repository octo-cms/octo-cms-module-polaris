<?php

namespace OctoCmsModule\Polaris\Http\Livewire;

use Illuminate\Support\Arr;
use Livewire\Component;
use OctoCmsModule\Sitebuilder\DTO\MenuItemDTO;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Services\MenuService;

/**
 * Class MainMenu
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Http\Livewire
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class MainMenu extends Component
{
    /**
     * MenuItemDTO
     *
     * @var MenuItemDTO[]
     */
    public $menuItems;

    /**
     * MenuService
     *
     * @var MenuService
     */
    protected $menuService;

    /**
     * Template
     *
     * @var string
     */
    protected $template;

    /**
     * Html
     *
     * @var string
     */
    protected $html;

    /**
     * Name mount
     *
     * @param MenuService $menuService MenuService
     *
     * @return void
     */
    public function mount(MenuService $menuService)
    {
        $this->menuService = $menuService;
        $result = $this->menuService->getMainMenu();
        $this->menuItems = Arr::get($result, 'items', []);
        $this->template = Arr::get($result, 'blade', 'main');
    }

    /**
     * Name doColumn
     *
     * @param MenuItemDTO $col   MenuItemDTO
     * @param int         $level Menu Level
     *
     * @return void
     */
    private function doColumn(MenuItemDTO $col, $level = 1)
    {

        if (count($col->children)) {
            $this->html .= '<div class="dropdown">' . PHP_EOL;

            $this->html .= $this->getLinkType($col, $level);

            $this->html .= '<div class="row dropdown-menu">' . PHP_EOL;
            $this->html .= '<div class="col-auto" data-dropdown-content>' . PHP_EOL;
            $this->html .= '<div class="dropdown-grid-menu">' . PHP_EOL;

            foreach ($col->children as $item) {
                $this->doColumn($item, $level++);
            }

            $this->html .= '</div>' . PHP_EOL;
            $this->html .= '</div>' . PHP_EOL;
            $this->html .= '</div>' . PHP_EOL;

            $this->html .= '</div>';
        } else {
            $this->html .= $this->getLinkType($col);
        }
    }

    /**
     * Name getLinkType
     *
     * @param MenuItemDTO $item  MenuItemDTO
     * @param int         $level Menu Level
     *
     * @return string
     */
    private function getLinkType(MenuItemDTO $item, $level = 1)
    {
        $link = "";
        $classAttributes = "";
        $textAttributes = 'role="button" data-toggle="dropdown-grid" aria-expanded="false"
        aria-haspopup="true" ';

        $children = (count($item->children) > 0);

        if ($level == 1) {
            $classAttributes = 'nav-link nav-item dropdown-toggle arrow-bottom';
        }

        if ($level == 2) {
            $classAttributes = 'dropdown-item';
        }

        /*
         *  Il menu corrente non ha figli
         */
        if (!$children) {
            $textAttributes = "";
            $classAttributes = "dropdown-item fade-page";

            /*
             * Il menu corrente non ha figli, e c'e' una eccezione se è al primo livello
             */
            if ($level == 1) {
                $classAttributes = "nav-link nav-item";
            }
        }



        switch ($item->type) {
            case MenuItem::TYPE_PAGE:
                $link .= '<a href="' . url($item->link) . '" class="' . $classAttributes . '">' .
                    $item->label . '</a>' . PHP_EOL;
                break;

            case MenuItem::TYPE_PARENT:
                $link .= '<a ' . $textAttributes . ' class="' . $classAttributes . '" href="#">' .
                    $item->label . '</a>' . PHP_EOL;
                break;

            case MenuItem::TYPE_EXTERNAL:
                $link .= '<a ' . $textAttributes . ' href="' . $item->link . '" class="' .
                    $classAttributes . '" target="_blank">' . $item->label . '</a>' . PHP_EOL;
                break;
        }

        return $link;
    }


    /**
     * Name getContentProperty
     *
     * @return string
     */
    public function getContentProperty()
    {
        $this->html = "";

        /**
         * MenuItemDTO
         *
         * @var MenuItemDTO $col
         */
        foreach ($this->menuItems as $col) {
            $this->html .= '<li class="nav-item">' . PHP_EOL;
            $this->doColumn($col);
            $this->html .= '</li>' . PHP_EOL;
        }

        return $this->html;
    }


    /**
     * Name render
     *
     * @return mixed
     */
    public function render()
    {
        return view()->first(
            [
                'livewire.' . $this->template,
                'polaris::livewire.' . $this->template,
            ]
        );
    }
}
