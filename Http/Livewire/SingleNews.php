<?php

declare(strict_types=1);

namespace OctoCmsModule\Polaris\Http\Livewire;

use Livewire\Component;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

use function trim;
use function view;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Polaris\Http\Livewire
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 */
class SingleNews extends Component
{
    public $targets = [];

    protected $livewireUtils;

    /**
     * Name mount
     *
     * @param LivewireUtils $livewireUtils LivewireUtils
     * @param array         $targets       array
     */
    public function mount(LivewireUtils $livewireUtils, array $targets = []): void
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * Name getNewsProperty
     *
     * @return array
     */
    public function getNewsProperty(): array
    {
        /**
         * News
         *
         * @var News[] $news
         */
        $entities = $this->livewireUtils->getEntities(
            News::with('newsLangs')
                ->with('pictures')
                ->with('categories')
                ->with('page')
                ->with('page.pageLangs'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        $picture = $entities[0]->getPictures()->first();

        $url = '';
        if (!empty($entities[0]->page->pageLangs)) {
            $url = trim(LanguageUtils::getLangValue($entities[0]->page->pageLangs, 'url'));
        }

        return [
            'id'                => $entities[0]->id,
            'title'             => LanguageUtils::getLangValue($entities[0]->newsLangs, 'title'),
            'date'              => $entities[0]->date->format('d/m/Y'),
            'short_description' => LanguageUtils::getLangValue($entities[0]->newsLangs, 'short_description'),
            'picture'           => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
            'author'            => $entities[0]->author,
            'url'               => $url,
        ];
    }

    /**
     * Name render
     *
     * @return mixed
     */
    public function render()
    {
        return view()->first(
            [
                'livewire.livewire.single-news',
                'polaris::livewire.single-news',
            ]
        );
    }
}
